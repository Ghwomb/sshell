#include <unistd.h>

int main(){
    
    //Typen för execvp:
    //int execvp(const char *file, char *const argv[]);

    char *argv[] = {"ls", "-l", "-h", "-a", NULL};

    execvp(argv[0],argv);

    return 0;
    
    }