#include <stdio.h>
#include <stdlib.h>
#include <unistd.h> //För access till POSIX-API:t, se https://en.wikipedia.org/wiki/Unistd.h

int main(){
    pid_t child_pid = fork();

    //Barnprocessen
    if(child_pid == 0){
        printf("### Child ###\nCurrent PID: %d and Child PID: %d\n",
               getpid(), child_pid);
    }
    else {
        sleep(1); // Vänta i en sekund.
        printf("### Parent ###\nCurrent PID: %d and Child PID: %d\n",
               getpid(), child_pid);
    }
 
    return 0;

}

