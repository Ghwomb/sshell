#include <stdio.h>
#include <stdlib.h>
#include <unistd.h> //För access till POSIX-API:t, se https://en.wikipedia.org/wiki/Unistd.h
#include <sys/wait.h>

int main(){
    pid_t child_pid;
    pid_t wait_result;
    int stat_loc;
    
    child_pid = fork();

    //Barnprocessen
    if(child_pid == 0){
        printf("### Barnet ###\nNuvarande PID: %d och barnets PID: %d\n",
               getpid(), child_pid);
        sleep(1); // Vänta i en sekund.
    }

    else {
        wait_result = waitpid(child_pid, &stat_loc, WUNTRACED);

        printf("### Föräldern ###\nNuvarande PID: %d och barnets PID: %d\n",
               getpid(), child_pid);
    }
 
    return 0;

}

